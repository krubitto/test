package org.apache.myfaces.blank.JavaMail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import java.util.Arrays;
import java.util.Properties;

public class Receive extends Mail {
    static final String ENCODING = "UTF-8";
    private Properties properties;

    public Receive(){
        properties = new Properties();
        properties.put("mail.pop3s.auth", "true");
        properties.put("mail.pop3s.host", "pop.gmail.com");
        properties.put("mail.pop3s.port", "995");
        properties.put("mail.pop3s.starttls.enable","true");
    }
    int receiveMessage(){
        try{
            int a = 0;
            Session session = Session.getInstance(properties, new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            Store store = session.getStore("pop3s");
            store.connect();
            Folder folder = store.getFolder("INBOX");
            folder.open(Folder.READ_WRITE);

            Message[] message = folder.getMessages();

            for (int i=0, n=message.length; i<n; i++)
                if(Arrays.equals(message[i].getFrom(), InternetAddress.parse("ilya.filinin@simbirsoft.com"))) {
                    System.out.println(i + ": " + Arrays.toString(message[i].getFrom()) + "\t" + message[i].getSubject());
                    a++;
                }

            folder.close(false);
            store.close();
            return a;
        }catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }
}
