package org.apache.myfaces.blank.JavaMail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

 public class Send extends Mail {

    private Properties properties;

     public Send() {
        properties = new Properties();
         properties.put("mail.smtp.auth", "true");
         properties.put("mail.smtp.host", "smtp.gmail.com");
         properties.put("mail.smtp.port", "587");
         properties.put("mail.smtp.starttls.enable","true");
    }

    void send(String subject, int text, String toEmail) {
        try{
            Session session = Session.getInstance(properties, new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
            message.setSubject(subject);
            message.setText(String.valueOf(text));

            Transport.send(message);
            System.out.println("Сообщение отправлено");
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

}
