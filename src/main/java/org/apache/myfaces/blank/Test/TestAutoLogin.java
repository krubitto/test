package org.apache.myfaces.blank.Test;

import org.apache.myfaces.blank.POM.AutoLogin;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;
import ru.stqa.selenium.factory.WebDriverPool;

public class TestAutoLogin {
    private AutoLogin autoLogin;
    private WebDriver webDriver;
    @BeforeMethod
    public void startBrowser() {
        webDriver = WebDriverPool.DEFAULT.getDriver(DesiredCapabilities.chrome());
    }

    @AfterSuite
    public void stopAllBrowsers() {
        WebDriverPool.DEFAULT.dismissAll();
    }

    @Test
    public void test1() {
        work();
    }
    @Test
    public void test2() {
        work();
    }
    @Test
    public void test3() {
        work();
    }

    private void work() {
        webDriver.manage().deleteAllCookies();
        webDriver.get("https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
        autoLogin = new AutoLogin(webDriver);
        autoLogin.setUserName("doggrey11997@gmail.com");
        autoLogin.clickUserName();
        autoLogin.setPassword("ZlZlZl****");
    }
}
