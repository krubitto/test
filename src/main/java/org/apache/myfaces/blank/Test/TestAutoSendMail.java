package org.apache.myfaces.blank.Test;

import org.apache.myfaces.blank.POM.AutoLogin;
import org.apache.myfaces.blank.POM.AutoSendMail;
import org.apache.myfaces.blank.POM.MailPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.stqa.selenium.factory.WebDriverPool;

public class TestAutoSendMail {
    private AutoSendMail autoSendMail;
    private WebDriver webDriver;
    @BeforeMethod
    public void startBrowser() {
        webDriver = WebDriverPool.DEFAULT.getDriver(DesiredCapabilities.chrome());
    }
    @AfterMethod
    public void sl(){autoSendMail.sleep();}

    @AfterSuite
    public void stopAllBrowsers() {
        WebDriverPool.DEFAULT.dismissAll();
    }

    @Test
    public void test1() {
        work();
    }
    @Test
    public void test2() { work(); }
    @Test
    public void test3() { work(); }

    private void work() {
        webDriver.get("https://accounts.google.com/signin/v2/identifier?continue=https%3A%2F%2Fmail.google.com%2Fmail%2F&service=mail&sacu=1&rip=1&flowName=GlifWebSignIn&flowEntry=ServiceLogin");
        autoSendMail = new AutoSendMail(webDriver);
        autoSendMail.setUserName("doggrey11997@gmail.com");
        autoSendMail.clickUserName();
        autoSendMail.setPassword("ZlZlZl*****");
        autoSendMail.sleep();
        autoSendMail.searchMail("ilya.filinin@simbirsoft.com");
        autoSendMail.sleep();
        int a = autoSendMail.task();
        autoSendMail.actionButtonSend();
        autoSendMail.setTo("portnov-kiril11997@mail.ru");
        autoSendMail.setTopic("тестовое задание Портнов");
        autoSendMail.setText("количество полученных писем:  " + String.valueOf(a));
        autoSendMail.sendMail();
        webDriver.manage().deleteAllCookies();

    }
}
