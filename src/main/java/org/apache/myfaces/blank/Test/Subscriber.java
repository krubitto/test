package org.apache.myfaces.blank.Test;

import java.io.Serializable;

public class Subscriber implements Serializable {
    private String FIO;
   private String numb;

    public Subscriber(String fio, String numb) {
        FIO = fio;
        this.numb = numb;
    }
    public Subscriber() {
        this.FIO ="asd";
        this.numb ="aa";
    }

    String getFIO() { return FIO; }

    void setFIO(String FIO) { this.FIO = FIO; }

    public String getNumb() { return numb; }

    void setNumb(String numb) { this.numb = numb; }

    @Override
    public String toString() {
        return "ФИО: "+ this.FIO +" телефон: " + this.numb;
    }
}
