package org.apache.myfaces.blank.Test;

import org.apache.myfaces.blank.POM.AutoLogin;
import org.apache.myfaces.blank.POM.MailPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;
import ru.stqa.selenium.factory.WebDriverPool;

public class TestMailPage{
    private MailPage mailPage;
    AutoLogin autoLogin;
    private WebDriver webDriver;

    @BeforeMethod
    public void startBrowser() {
        webDriver = WebDriverPool.DEFAULT.getDriver(DesiredCapabilities.chrome());
    }

    @AfterSuite
    public void stopAllBrowsers() {
        WebDriverPool.DEFAULT.dismissAll();
    }

    @Test
    public void test1() {
        work();
    }

    private void work() {
        webDriver.manage().deleteAllCookies();
        webDriver.get("https://mail.google.com/mail/u/0/#inbox");
        autoLogin = new AutoLogin(webDriver);
        autoLogin.loginAutoLogin("doggrey11997@gmail.com","ZlZlZl*****");
        mailPage = new MailPage(webDriver);
        mailPage.sleep();
        mailPage.searchMail("ilya.filinin@simbirsoft.com");
        mailPage.sleep();
        int a = mailPage.task();
        mailPage.actionButtonSend();
        mailPage.setTo("portnov-kiril11997@mail.ru");
        mailPage.setTopic("тестовое задание Портнов");
        mailPage.setText("количество полученных писем:  " + String.valueOf(a));
        mailPage.sendMail();
    }
}
