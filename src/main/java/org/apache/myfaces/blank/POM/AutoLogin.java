package org.apache.myfaces.blank.POM;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class AutoLogin  {
    private WebDriver driver;
    private By emile_phone = By.xpath("//input[@id='identifierId']");
    private By password;
    private By next = By.id("identifierNext");
//    private By next_pas = By.className("RveJvd snByac");
    private String name_pass = "password";

    public AutoLogin(WebDriver webdriver){
        driver =webdriver;
    }
    public void setUserName(String username){ driver.findElement(emile_phone).sendKeys(username); }
    public void clickUserName(){
        driver.findElement(next).click();
    }
    public void setPassword(String pass){
        password = By.name(name_pass);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(password));
        driver.findElement(password).sendKeys(pass,Keys.ENTER);
    }
//    public void clickPassword(){
//        driver.findElement(next_pas).click();
//    }
    public void loginAutoLogin(String user, String pass){
        this.setUserName(user);
        this.clickUserName();
        this.setPassword(pass);
//        this.clickPassword();
    }
    public void close(){
        driver.quit();
    }
}
