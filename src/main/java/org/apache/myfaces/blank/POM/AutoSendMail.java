package org.apache.myfaces.blank.POM;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class AutoSendMail {
    private WebDriver driver;
    private By emile_phone = By.xpath("//input[@id='identifierId']");
    private By password;
    private By next = By.id("identifierNext");
    private String name_pass = "password";
    private By search = By.name("q");
    private By value1 = By.id(":ak");
    private By value2 = By.className("ts");
    private By block = By.className("aic");
    private By to = By.name("to");
    private By topic = By.name("subjectbox");
    private By body = By.id(":ey");
    private By button = By.id(":dj");

    public AutoSendMail(WebDriver webdriver){
        driver =webdriver;
    }
    public void setUserName(String username){ driver.findElement(emile_phone).sendKeys(username); }
    public void clickUserName(){
        driver.findElement(next).click();
    }
    public void setPassword(String pass){
        password = By.name(name_pass);
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(password));
        driver.findElement(password).sendKeys(pass, Keys.ENTER);
    }
    public void searchMail(String adress){
        driver.findElement(search).sendKeys(adress, Keys.ENTER);
    }
    public int task(){
        int a =0;
        List<WebElement> srch = driver.findElement(value1).findElements(value2);
        return a = Integer.parseInt(srch.get(srch.size()-1).getText());
    }
    public void actionButtonSend(){
        Dimension size = driver.findElement(block).getSize();
        Actions action =new Actions(driver);
        action.moveToElement(driver.findElement(block),size.getWidth() - 130,
                size.getHeight() - 40).click().build().perform();
    }
    public void setTo(String adress){
        driver.findElement(to).sendKeys(adress);
    }
    public void setTopic(String text){
        driver.findElement(topic).sendKeys(text);
    }
    public void setText(String text){
        driver.findElement(body).sendKeys(text);
    }
    public void sendMail(){
        driver.findElement(button).click();
    }
    public void sleep(){
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public void sendAutoSendMail(String user, String pass,String adressFrom, String to,String top, String text){
        this.setUserName(user);
        this.clickUserName();
        this.setPassword(pass);
        this.sleep();
        this.searchMail(adressFrom);
        this.sleep();
        int a = this.task();
        this.actionButtonSend();
        this.setTo(to);
        this.setTopic(top);
        this.setText(text + String.valueOf(a));
        this.sendMail();
    }
}
